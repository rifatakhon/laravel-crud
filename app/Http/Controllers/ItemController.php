<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;

use DB;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::table('items')->get();
        return view('item.home', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       DB::table('items')->insert([
        'name' => $request->name,
        'price' => $request->price,
        'publisher' => $request->publisher,
        'status'    => '1',
       ]) ;

       return back()->with('save', 'Success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // dd("abc");
        $items = DB::table('items')->where('id', $id)->first();
        return view('item/edit',['items' => $items]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        DB::table('items')->where('id', $request->id)->update([
            'name' => $request->name,
            'price' => $request->price,
            'publisher' => $request->publisher,
            'status'    => '1',
        ]);

        return back()->with('update', 'updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('items')->where('id', $id)->delete();

        return back()->with('delete', 'Deleted Successfully');

    }
}
