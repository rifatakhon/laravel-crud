<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/item', [App\Http\Controllers\ItemController::class, 'index'])->name('show');
Route::post('/store', [App\Http\Controllers\ItemController::class, 'store'])->name('itam.add');
Route::get('/edit/{id}', [App\Http\Controllers\ItemController::class, 'edit'])->name('edit.item');
Route::post('/update', [App\Http\Controllers\ItemController::class, 'update'])->name('itam.update');
Route::get('/delete/{id}', [App\Http\Controllers\ItemController::class, 'destroy']);